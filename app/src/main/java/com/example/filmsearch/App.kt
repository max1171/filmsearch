package com.example.filmsearch

import android.app.Application
import com.example.filmsearch.data.FilmRepository
import com.example.filmsearch.data.FilmService
import com.example.filmsearch.domain.KinoPoiskInteractor
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class App : Application() {
    lateinit var filmService: FilmService
    lateinit var konoPoiskInteractor: KinoPoiskInteractor
    var filmRepository = FilmRepository()

    companion object {
        lateinit var instance: App
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        initRetrofit()
        initInteractor()
    }

    private fun initInteractor() {
        konoPoiskInteractor = KinoPoiskInteractor(filmService, filmRepository)
    }

    private fun initRetrofit() {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BASIC

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor { chain ->
                return@addInterceptor chain.proceed(
                    chain
                        .request()
                        .newBuilder()
                        .addHeader("X-API-KEY", "51ebe8b4-a4d2-49ce-ad54-cba690766d4b")
                        .build()
                )
            }
            .addInterceptor(logging)
            .build()


//        val okHttpClient = OkHttpClient.Builder()
//            .addInterceptor(logging)
//            .build()

        filmService = Retrofit.Builder()
            .baseUrl("https://kinopoiskapiunofficial.tech/api/v2.2/films/")
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .build()
            .create(FilmService::class.java)
    }
}