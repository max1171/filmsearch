package com.example.filmsearch.domain

import android.util.Log
import com.example.filmsearch.data.FilmService
import com.example.filmsearch.data.FilmRepository
import com.example.filmsearch.data.entity.FilmModel
import com.example.filmsearch.data.entity.InformationFilmModel
import com.example.filmsearch.presentation.view.InformationFilmItem
import com.example.filmsearch.presentation.view.recycler.FilmItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class KinoPoiskInteractor(
    private val filmService: FilmService,
    private val filmRepository: FilmRepository
) {

    fun getFilmTop(page: Int, callback: GetFilmCallback) {
        filmService.getFilmTop(page).enqueue(object : Callback<FilmModel> {
            override fun onResponse(call: Call<FilmModel>, response: Response<FilmModel>) {
                if (response.isSuccessful) {
                    val list = migrateFilmModelInFilmItems(response.body()!!)
                    val countPage = response.body()!!.pagesCount

                    filmRepository.addCacheFilmList(list, countPage)

                    callback.onSuccess(filmRepository.getCacheFilm, filmRepository.getCachePageCount)
                } else {
                    callback.onError(response.code().toString())
                }
            }

            override fun onFailure(call: Call<FilmModel>, t: Throwable) {
                callback.onError("Network error probably...")
            }

        })
    }

    fun getDetailInformationFilm(filmId: Int, callback:GetDetailFilmInformation){
        filmService.getFilmInformation(filmId).enqueue(object : Callback<InformationFilmModel>{
            override fun onResponse(
                call: Call<InformationFilmModel>,
                response: Response<InformationFilmModel>
            ) {
                if (response.isSuccessful){
                    val detail = migrationInfFilmModelInInfFilmItem(response.body()!!)
                    filmRepository.addDetailInformationFilm(detail)
                    callback.onSuccess(filmRepository.getDetailInformationFilm)
                } else {
                    callback.onError(response.code().toString())
                }
            }

            override fun onFailure(call: Call<InformationFilmModel>, t: Throwable) {
                callback.onError("Network error probably...")
            }

        })
    }

    fun migrateFilmModelInFilmItems(filmModel: FilmModel): ArrayList<FilmItem> {
        val filmsItem = arrayListOf<FilmItem>()
        filmModel.films.forEach { film ->
            filmsItem.add(
                FilmItem(
                    film.filmId,
                    film.nameRu,
                    film.posterUrlPreview,
                    film.posterUrl,
                    film.year,
                    film.filmLength,
                    film.countries.toString().replace("[", "").replace("]", ""),
                    film.genres.toString().replace("[", "").replace("]", ""),
                    film.rating,
                    false
                )
            )
        }
        return filmsItem
    }

    fun migrationInfFilmModelInInfFilmItem(inf: InformationFilmModel): InformationFilmItem{
        return InformationFilmItem(
            inf.nameRu,
            inf.posterUrl,
            inf.description,
            inf.shortDescription,
            inf.ratingKinopoisk,
            inf.webUrl
        )
    }

    interface GetFilmCallback {
        fun onSuccess(films: ArrayList<FilmItem>, countPage: Int)
        fun onError(error: String)
    }

    interface GetDetailFilmInformation{
        fun onSuccess(detailInformationFilm: InformationFilmItem)
        fun onError(error: String)
    }
}