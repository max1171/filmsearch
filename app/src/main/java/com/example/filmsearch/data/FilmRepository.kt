package com.example.filmsearch.data

import com.example.filmsearch.presentation.view.InformationFilmItem
import com.example.filmsearch.presentation.view.recycler.FilmItem

class FilmRepository {
    private var cachedTopListFilms = arrayListOf<FilmItem>()
    private var pageCount = 0
    private var filmDetailInformation: InformationFilmItem? = null

    val getCacheFilm: ArrayList<FilmItem>
        get() = cachedTopListFilms

    val getCachePageCount: Int
        get() = pageCount

    val getDetailInformationFilm: InformationFilmItem
        get() = filmDetailInformation!!

    fun addCacheFilmList(films: ArrayList<FilmItem>, countPage: Int) {
        cachedTopListFilms.addAll(films)
        pageCount = countPage
    }

    fun addDetailInformationFilm(inf: InformationFilmItem){
        filmDetailInformation = inf
    }
}