package com.example.filmsearch.data.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
class FilmModel(
    var pagesCount: Int,
    var films: MutableList<Films>
) : Parcelable

@Parcelize
class Films(
    var posterUrlPreview: String,
    var posterUrl: String,
    var filmId: Int,
    var nameRu: String,
    var year: String,
    var filmLength: String,
    val countries: MutableList<Country>,
    var genres: MutableList<Genre>,
    var rating: Float
) : Parcelable

@Parcelize
class Country(var country: String) : Parcelable {
    override fun toString(): String {
        return country
    }
}

@Parcelize
class Genre(var genre: String) : Parcelable {
    override fun toString(): String {
        return genre
    }
}