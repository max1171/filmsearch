package com.example.filmsearch.data


import com.example.filmsearch.data.entity.FilmModel
import com.example.filmsearch.data.entity.InformationFilmModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface FilmService {
    @GET("top")
    fun getFilmTop(@Query("page") page: Int): Call<FilmModel>

    @GET("{id}")
    fun getFilmInformation(@Path("id") id: Int): Call<InformationFilmModel>
}