package com.example.filmsearch.data.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class InformationFilmModel(
    var nameRu: String,
    var posterUrl: String,
    var description: String,
    var shortDescription: String,
    var ratingKinopoisk:Float,
    var webUrl: String
): Parcelable