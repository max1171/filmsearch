package com.example.filmsearch.presentation.view

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.filmsearch.R
import com.example.filmsearch.presentation.view.recycler.FilmAdapter
import com.example.filmsearch.presentation.view.recycler.FilmItem
import com.example.filmsearch.presentation.viewModel.FilmListViewModel

class FilmListFragment(var sharedPreference: SharedPreferences) : Fragment() {

    companion object {
        const val TAG = "FilmListFragment"
    }

    var listener: OnFilmClickListener? = null
    private var pageCount = 0
    private var nextPage = 1
    private var filmList = arrayListOf<FilmItem>()
    lateinit var filmAdapter: FilmAdapter
    lateinit var recycler: RecyclerView
    lateinit var progressbar: ProgressBar
    lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var animationUtils: AnimationUtils
    lateinit var layoutManager: LinearLayoutManager
    private lateinit var sharedPreferenceEditor: SharedPreferences.Editor


    private val viewModel: FilmListViewModel by lazy {
        ViewModelProvider(requireActivity()).get(FilmListViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_film_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

//        sharedPreference = requireActivity().getSharedPreferences("preference", Context.MODE_PRIVATE)
        sharedPreferenceEditor = sharedPreference.edit()
        animationUtils = AnimationUtils(view.context)
        progressbar = view.findViewById(R.id.progressbar)
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout)
        layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recycler = view.findViewById(R.id.recyclerView)
        recycler.layoutManager = layoutManager
        initializeAdapter()
        recycler.adapter = filmAdapter
        recycler.addItemDecoration(ItemOffsetDecoration(50))
        swipeRefreshLayout.setOnRefreshListener {
            swipeRefreshLayout.isRefreshing = false
        }

        val positionDetailInformation = sharedPreference.getInt("POSITION_DETAIL_INFORMATION",0)
        if (positionDetailInformation > 0) {
            layoutManager.scrollToPosition(positionDetailInformation)
        }

        initializeObserverFilmList()
        initScrollListener()
        if(viewModel.repos.value?.isNullOrEmpty() == null){
            viewModel.loadTopFilmList(nextPage)
            nextPage++
        }
    }

    private fun initScrollListener() {
        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (progressbar.visibility == View.GONE) {
                    if (layoutManager.findLastVisibleItemPosition() == filmList.size - 1) {
                        if (pageCount >= nextPage) {
                            progressbar.visibility = View.VISIBLE
                            viewModel.loadTopFilmList(nextPage)
                            nextPage++
                        }
                    }
                }

            }
        })
        recycler.post(Runnable {
            kotlin.run {
                recycler.adapter?.notifyItemRangeChanged(
                    filmList.size - 20,
                    filmList.size
                )
            }
        })
    }

    private fun initializeObserverFilmList() {
        viewModel.repos.observe(this.viewLifecycleOwner, Observer<ArrayList<FilmItem>> { repos ->
            filmList = repos
            filmAdapter.setItems(filmList)
            progressbar.visibility = View.GONE
        })
        viewModel.countPage.observe(this.viewLifecycleOwner, Observer<Int> { count ->
            pageCount = count
        })
        viewModel.error.observe(this.viewLifecycleOwner, Observer<String> { error ->
            Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
        })
    }

    private fun initializeAdapter() {
       filmAdapter = FilmAdapter(LayoutInflater.from(context),
            object : FilmAdapter.OnClickListenerFilm {

                override fun onFilmClick(filmItem: FilmItem, position: Int) {
                    sharedPreferenceEditor.apply {
                        this.putInt("POSITION_DETAIL_INFORMATION", position)
                    }
                    listener?.onFilmClick(filmItem, position)
                }

                override fun onLikeClick(
                    filmItem: FilmItem,
                    isLike: CheckBox,
                    noteLike: CheckBox,
                    position: Int
                ) {
                    filmItem.like = !filmItem.like
                    filmList[position] = filmItem
                    filmAdapter.notifyItemChanged(position)
//                    animationUtils.animatorSetInvisibleLike(isLike, noteLike)
                }
            })
    }

    class MyActivityObserver(
        private val update: () -> Unit
    ) : DefaultLifecycleObserver {

        override fun onCreate(owner: LifecycleOwner) {
            super.onCreate(owner)
            owner.lifecycle.removeObserver(this)
            update()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.lifecycle?.addObserver(MyActivityObserver {
            if (activity is OnFilmClickListener) {
                listener = activity as OnFilmClickListener
            } else {
                throw Exception("Activity must implement OnFilmClickListener")
            }
        })
    }

    interface OnFilmClickListener {
        fun onFilmClick(filmItem: FilmItem, position: Int)
        fun onLikeClick(
            filmItem: FilmItem,
            isLike: CheckBox,
            position: Int
        )
    }
}