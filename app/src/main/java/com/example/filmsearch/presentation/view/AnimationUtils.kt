package com.example.filmsearch.presentation.view

import android.animation.AnimatorInflater
import android.content.Context
import android.widget.CheckBox
import com.example.filmsearch.R

class AnimationUtils (var context: Context){
    fun animatorSetInvisibleLike(isLike: CheckBox, noteLike: CheckBox) {
        AnimatorInflater.loadAnimator(context, R.animator.animation_invisible_like_1).apply {
            setTarget(noteLike)
            start()
        }
        AnimatorInflater.loadAnimator(context, R.animator.animation_invisible_like_2).apply {
            setTarget(isLike)
            start()
        }
    }

    fun animatorSetVisibleLike(isLike: CheckBox, noteLike: CheckBox) {
        AnimatorInflater.loadAnimator(context, R.animator.animator_visible_like_1).apply {
            setTarget(noteLike)
            start()
        }
        AnimatorInflater.loadAnimator(context, R.animator.animator_visible_like_2).apply {
            setTarget(isLike)
            start()
        }
    }
}