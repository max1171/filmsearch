package com.example.filmsearch.presentation.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.filmsearch.App
import com.example.filmsearch.domain.KinoPoiskInteractor
import com.example.filmsearch.presentation.view.InformationFilmItem

class DetailFilmViewModel: ViewModel() {
    private val kinoPoiskInteractor = App.instance.konoPoiskInteractor
    private var detailInformationFilm = MutableLiveData<InformationFilmItem>()
    private val errorLifeData = MutableLiveData<String>()

    val detailInformation: LiveData<InformationFilmItem>
        get() = detailInformationFilm

    val error: LiveData<String>
        get() = errorLifeData

    fun loadDetailInformationFilm(id: Int) {
        kinoPoiskInteractor.getDetailInformationFilm(id, object : KinoPoiskInteractor.GetDetailFilmInformation{
            override fun onSuccess(detailInformationFilm: InformationFilmItem) {
                this@DetailFilmViewModel.detailInformationFilm.postValue(detailInformationFilm)
            }

            override fun onError(error: String) {
                errorLifeData.postValue(error)
            }
        })
    }
}