package com.example.filmsearch.presentation.view

import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.filmsearch.R
import com.example.filmsearch.presentation.view.recycler.FilmAdapter
import com.example.filmsearch.presentation.view.recycler.FilmItem
import com.example.filmsearch.presentation.viewModel.FilmListViewModel

class FilmFavoriteListFragment() : Fragment() {

    companion object {
        val TAG = "FilmFavoriteListFragment"
    }

    private var filmList = arrayListOf<FilmItem>()
    private var favoriteFilm = mutableListOf<FilmItem>()
    private lateinit var animationUtils: AnimationUtils
    lateinit var adapter: FilmAdapter
    lateinit var recycler: RecyclerView
    lateinit var layoutManager: LinearLayoutManager
    private val viewModel: FilmListViewModel by lazy {
        ViewModelProvider(requireActivity()).get(FilmListViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_favorit_film_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initializeObserver()

        animationUtils = AnimationUtils(view.context)
        recycler = view.findViewById(R.id.recyclerViewFavorite)
        layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        initialAdapter()
        recycler.adapter = adapter
        recycler.layoutManager = layoutManager
        recycler.addItemDecoration(ItemOffsetDecoration(50))
    }

    private fun initialAdapter(){
         adapter =
            FilmAdapter(LayoutInflater.from(context), object : FilmAdapter.OnClickListenerFilm {
                override fun onFilmClick(filmItem: FilmItem, position: Int) {
                    TODO("Not yet implemented")
                }

                override fun onLikeClick(
                    filmItem: FilmItem,
                    isLike: CheckBox,
                    noteLike: CheckBox,
                    position: Int
                ) {
                    filmItem.like = !filmItem.like
//                    animationUtils.animatorSetInvisibleLike(isLike, noteLike)
                    filmList[position] = filmItem
                    adapter.notifyItemChanged(position)
                    loadFavoriteFilm()
                }
            })
    }

    private fun initializeObserver(){
        viewModel.repos.observe(this.viewLifecycleOwner, Observer<ArrayList<FilmItem>> { repos ->
            filmList = repos
            loadFavoriteFilm()
        })
    }

    fun loadFavoriteFilm(){
        favoriteFilm = filmList.filter { it.like }.toMutableList()
        adapter.setItems(favoriteFilm as ArrayList<FilmItem>)
    }
}