package com.example.filmsearch.presentation.view.recycler

data class FilmItem(
    var id: Int,
    var nameRu: String,
    var posterUrlPreview: String,
    var posterUrl: String,
    var year: String,
    var filmLength: String,
    var countries: String,
    var genres: String,
    var ratingNumber: Float,
    var like: Boolean
)