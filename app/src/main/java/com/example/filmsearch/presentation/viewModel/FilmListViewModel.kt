package com.example.filmsearch.presentation.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.filmsearch.App
import com.example.filmsearch.domain.KinoPoiskInteractor
import com.example.filmsearch.presentation.view.recycler.FilmItem

class FilmListViewModel : ViewModel() {
    private val filmLifeData = MutableLiveData<ArrayList<FilmItem>>()
    private val errorLifeData = MutableLiveData<String>()
    private var pageCount = MutableLiveData<Int>()

    private val kinoPoiskInteractor = App.instance.konoPoiskInteractor

    val repos: LiveData<ArrayList<FilmItem>>
        get() = filmLifeData

    val countPage: LiveData<Int>
        get() = pageCount

    val error: LiveData<String>
        get() = errorLifeData

    fun loadTopFilmList(page: Int) {
        kinoPoiskInteractor.getFilmTop(page, object : KinoPoiskInteractor.GetFilmCallback {
            override fun onSuccess(films: ArrayList<FilmItem>, countPage: Int) {
                pageCount.postValue(countPage)
                filmLifeData.postValue(films)
            }

            override fun onError(error: String) {
                errorLifeData.postValue(error)
            }
        })
    }
}