package com.example.filmsearch.presentation.view.recycler

import android.util.Log
import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.filmsearch.R

class FilmItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val posterUrlPreview: ImageView = itemView.findViewById(R.id.posterUrlPreview)
    private val nameRu: TextView = itemView.findViewById(R.id.nameRu)
    private val year: TextView = itemView.findViewById(R.id.tvYears)
    private val filmLength: TextView = itemView.findViewById(R.id.tvFilmLength)
    private val countries: TextView = itemView.findViewById(R.id.tvCountries)
    private val genres: TextView = itemView.findViewById(R.id.tvGenre)
    private val rating: RatingBar = itemView.findViewById(R.id.ivRating)
    private val ratingNumber: TextView = itemView.findViewById(R.id.tvRatingNumber)

    private val isLike: CheckBox = itemView.findViewById(R.id.like)
    private val noteLike: CheckBox = itemView.findViewById(R.id.note_like)

    fun bind(item: FilmItem) {
        nameRu.text = item.nameRu
        year.text = "Год: " + item.year
        filmLength.text = "Продолжительность: " + item.filmLength
        countries.text = "Страна: " + item.countries
        genres.text = "Жанр: " + item.genres
        rating.rating = item.ratingNumber
        ratingNumber.text = item.ratingNumber.toString()

        if (item.like){
            isLike.setBackgroundResource(R.drawable.ic_baseline_favorite_24)
        }else{
            isLike.setBackgroundResource(R.drawable.ic_baseline_favorite_border_24)
        }

//        Picasso.get()
//            .load(item.imgFilm)
//            .placeholder(R.drawable.ic_image)
//            .error(R.drawable.ic_error_150)
//            .resizeDimen(R.dimen.image_size, R.dimen.image_size)
//            .centerCrop()
//            .into(imgFilm)

        Glide.with(posterUrlPreview.context)
            .load(item.posterUrlPreview)
            .placeholder(R.drawable.ic_image)
            .error(R.drawable.ic_error_150)
            .centerCrop()
            .transform(RoundedCorners(30))
            .into(posterUrlPreview)
    }
}