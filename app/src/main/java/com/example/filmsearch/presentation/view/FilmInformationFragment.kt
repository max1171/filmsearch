package com.example.filmsearch.presentation.view

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.example.filmsearch.R
import com.example.filmsearch.presentation.view.recycler.FilmItem
import com.example.filmsearch.presentation.viewModel.DetailFilmViewModel
import com.google.android.material.snackbar.Snackbar

class FilmInformationFragment : Fragment() {
    private val REQUEST_CODE_GRANTED = 0
    lateinit var posterUrl: ImageView
    lateinit var rating: RatingBar
    lateinit var ratingNumber: TextView
    lateinit var description: TextView
    lateinit var progressbarLoadImage: ProgressBar
    private var informationFilmItem: InformationFilmItem? = null
    private val viewModel: DetailFilmViewModel by lazy {
        ViewModelProvider(requireActivity()).get(DetailFilmViewModel::class.java)
    }

    companion object {
        const val TAG = "FilmInformationFragment"
        const val FILM_ID = "FILM_ID"

        fun newInstance(filmTitle: FilmItem): FilmInformationFragment {
            val fragment = FilmInformationFragment()
            val bundle = Bundle()
            bundle.putInt(FILM_ID, filmTitle.id)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_film_information, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        posterUrl = view.findViewById(R.id.posterUrl)
        rating = view.findViewById(R.id.ivRating)
        ratingNumber = view.findViewById(R.id.tvRatingNumber)
        description = view.findViewById(R.id.tv_description)
        progressbarLoadImage = view.findViewById(R.id.progressbarLoadImage)
        val filmId = arguments?.getInt(FILM_ID)
        initializeObserver()
        viewModel.loadDetailInformationFilm(filmId!!)
        sendMessageFilm()
    }

    private fun initializeObserver() {
        viewModel.detailInformation.observe(
            this.viewLifecycleOwner,
            Observer<InformationFilmItem> { detailInformation ->
                if (viewLifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED) {
                    informationFilmItem = detailInformation

                    view?.findViewById<Toolbar>(R.id.toolbar)?.title = informationFilmItem?.nameRu
                    rating.rating = informationFilmItem!!.ratingKinopoisk
                    ratingNumber.text = informationFilmItem?.ratingKinopoisk.toString()

                    if (!TextUtils.isEmpty(informationFilmItem?.description)) {
                        val text = "${informationFilmItem?.description} "
                        description.text = text
                    }
                    if (!TextUtils.isEmpty(informationFilmItem?.shortDescription)) {
                        val text =
                            "${description.text}" + "${informationFilmItem?.shortDescription}"
                        description.text = text
                    }
                    Glide.with(posterUrl.context)
                        .load(informationFilmItem!!.posterUrl)
                        .placeholder(R.drawable.ic_image_500)
                        .error(R.drawable.ic_error_500)
                        .centerCrop()
                        .into(posterUrl)
                    progressbarLoadImage.visibility = View.GONE
                }
            })

        viewModel.error.observe(this.viewLifecycleOwner, Observer<String> { error ->
            Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
        })
    }

    private fun sendMessageFilm() {
        view?.findViewById<View>(R.id.send_message_friends)?.setOnClickListener {
            if (isPermissionsAllowed()){
                val textMessage =
                    "Посмотри фильм" + " ${informationFilmItem?.nameRu}" + " ${informationFilmItem?.webUrl}"
                val sendIntent = Intent(Intent.ACTION_SEND)
                sendIntent.type = "text/plain"
                sendIntent.putExtra(Intent.EXTRA_TEXT, textMessage)
                startActivity(sendIntent)
            }
        }
    }

    private fun isPermissionsAllowed(): Boolean {
        return if(ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.READ_CONTACTS
            ) == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            askForPermissions()
            false
        }
    }

    private fun askForPermissions() {
        return if (ActivityCompat.shouldShowRequestPermissionRationale(
                requireActivity(),
                Manifest.permission.READ_CONTACTS
            )
        ) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.READ_CONTACTS),
                REQUEST_CODE_GRANTED
            )
        } else {
            Snackbar.make(requireView(), "Перейдите в настройки системы и откройте доступ к контактам",Snackbar.LENGTH_SHORT).show()
        }
    }
}