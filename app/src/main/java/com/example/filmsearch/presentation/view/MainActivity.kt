package com.example.filmsearch.presentation.view

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.CheckBox
import androidx.appcompat.app.AppCompatActivity
import com.example.filmsearch.R
import com.example.filmsearch.presentation.view.recycler.FilmItem
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity(), FilmListFragment.OnFilmClickListener {

    private lateinit var sharedPreference: SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initialNavigationActionBar()
        sharedPreference = getSharedPreferences("preference", Context.MODE_PRIVATE)
        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.fragment_container,
                FilmListFragment(sharedPreference),
                FilmListFragment.TAG
            )
            .commit()
    }

    override fun onFilmClick(filmItem: FilmItem, position: Int) {
        supportActionBar?.hide()

        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.fragment_container,
                FilmInformationFragment.newInstance(filmItem),
                FilmInformationFragment.TAG
            )
            .addToBackStack(null)
            .commit()
    }

    override fun onLikeClick(
        filmItem: FilmItem,
        isLike: CheckBox,
        position: Int,
    ) {
    }

    private fun initialNavigationActionBar() {
        supportActionBar

        findViewById<BottomNavigationView>(R.id.navigation).setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.favorite_page -> {
                        supportFragmentManager
                            .beginTransaction()
                            .replace(
                                R.id.fragment_container,
                                FilmFavoriteListFragment(),
                                FilmFavoriteListFragment.TAG
                            )
                            .addToBackStack(null)
                            .commit()
                }
                R.id.home_page -> {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(
                            R.id.fragment_container,
                            FilmListFragment(sharedPreference),
                            FilmListFragment.TAG
                        )
                        .commit()
                }
            }
            true
        }
    }

//    override fun onBackPressed() {
//        MaterialAlertDialogBuilder(this)
//            .setTitle(resources.getString(R.string.title))
//            .setMessage(resources.getString(R.string.supporting_text))
////            .setNeutralButton(resources.getString(R.string.cancel)) { dialog, which ->
////                // Respond to neutral button press
////            }
//            .setNegativeButton(resources.getString(R.string.decline)) { dialog, which ->
//                dialog.cancel()
//            }
//            .setPositiveButton(resources.getString(R.string.accept)) { dialog, which ->
//                super.onBackPressed()
//            }
//            .show()
//    }
}