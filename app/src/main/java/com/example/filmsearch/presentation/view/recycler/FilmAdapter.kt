package com.example.filmsearch.presentation.view.recycler

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.recyclerview.widget.RecyclerView
import com.example.filmsearch.R

class FilmAdapter(
    private val inflater: LayoutInflater,
    private val listener: OnClickListenerFilm
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val items = arrayListOf<FilmItem>()
    private var bool = true

    fun setItems(films: ArrayList<FilmItem>) {
        items.clear()
        items.addAll(films)
        Log.d("TAG", "setItems: ")
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return FilmItemViewHolder(inflater.inflate(R.layout.film_item, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is FilmItemViewHolder) {
            val item = items[position]
            holder.bind(item)
            holder.itemView.setOnClickListener { listener.onFilmClick(items[position], position) }
            val like = holder.itemView.findViewById<CheckBox>(R.id.like)
            val noteLike = holder.itemView.findViewById<CheckBox>(R.id.note_like)
            like.setOnClickListener {
                listener.onLikeClick(
                    items[position],
                    like,
                    noteLike,
                    position
                )
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    interface OnClickListenerFilm {
        fun onFilmClick(filmItem: FilmItem, position: Int)
        fun onLikeClick(filmItem: FilmItem, isLike: CheckBox, noteLike: CheckBox, position: Int)
    }
}