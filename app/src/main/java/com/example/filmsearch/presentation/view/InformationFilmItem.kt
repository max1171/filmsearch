package com.example.filmsearch.presentation.view

data class InformationFilmItem(
    val nameRu: String,
    val posterUrl: String,
    val description: String,
    val shortDescription: String?,
    val ratingKinopoisk: Float,
    val webUrl: String
)